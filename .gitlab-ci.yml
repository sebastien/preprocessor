variables:
  TERM: linux
  MINGW64_BOOST_VERSION: 1.85.0-2
  WGET_OPTIONS: '--no-verbose --no-use-server-timestamps --retry-connrefused --retry-on-host-error'
  # To ensure that "false && true" fails, see https://gitlab.com/gitlab-org/gitlab-runner/-/issues/25394#note_412609647
  FF_ENABLE_BASH_EXIT_CODE_CHECK: 'true'

build_linux_x86_64:
  stage: build
  script:
    - meson setup -D buildtype=release build
    - meson compile -C build -v
  artifacts:
    paths:
      - build/src/dynare-preprocessor

build_linux_arm64:
  stage: build
  script:
    - meson setup -D buildtype=release --cross-file scripts/linux-arm64-cross.ini build
    - meson compile -C build -v
  artifacts:
    paths:
      - build/src/dynare-preprocessor

build_windows_x86_64:
  stage: build
  script:
    - mkdir -p tarballs
    - '[[ -f tarballs/mingw-w64-x86_64-boost-$MINGW64_BOOST_VERSION-any.pkg.tar.zst ]] || wget $WGET_OPTIONS -P tarballs http://repo.msys2.org/mingw/x86_64/mingw-w64-x86_64-boost-$MINGW64_BOOST_VERSION-any.pkg.tar.zst'
    - mkdir -p deps
    - tar xf tarballs/mingw-w64-x86_64-boost-$MINGW64_BOOST_VERSION-any.pkg.tar.zst --directory deps
    - echo -e "[properties]\nboost_root = '$(pwd)/deps/mingw64/'" > boost.ini
    - meson setup -D buildtype=release --cross-file scripts/windows-cross.ini --cross-file boost.ini build
    - meson compile -C build -v
  cache:
    # This cache is shared between all branches, to save space
    key: $CI_JOB_NAME
    paths:
      - tarballs/
  artifacts:
    paths:
      - build/src/dynare-preprocessor.exe

build_macos_x86_64:
  stage: build
  tags:
    - macOS
  script:
    # Workaround for bug in Xcode 15.3 which does not include m4
    # See https://github.com/Homebrew/homebrew-core/issues/165388 and https://trac.macports.org/ticket/69639
    - export PATH="/usr/local/opt/m4/bin/:$PATH"
    - arch -x86_64 meson setup -D buildtype=release --native-file scripts/homebrew-native-x86_64.ini build
    - arch -x86_64 meson compile -C build -v
  artifacts:
    paths:
      - build/src/dynare-preprocessor

build_macos_arm64:
  stage: build
  tags:
    - macOS
  script:
    - export PATH="/opt/homebrew/bin:$PATH"
    # Workaround for bug in Xcode 15.3 which does not include m4
    # See https://github.com/Homebrew/homebrew-core/issues/165388 and https://trac.macports.org/ticket/69639
    - export PATH="/opt/homebrew/opt/m4/bin/:$PATH"
    - arch -arm64 meson setup -D buildtype=release --native-file scripts/homebrew-native-arm64.ini build
    - arch -arm64 meson compile -C build -v
  artifacts:
    paths:
      - build/src/dynare-preprocessor

test_clang_format:
  stage: test
  script:
    - meson setup build-clang-format
    - ninja -C build-clang-format clang-format-check
  needs: []

test_clang_tidy:
  stage: test
  script:
    - mkdir -p ~/.local/bin && ln -s /usr/bin/clang-tidy-16 ~/.local/bin/clang-tidy
    - export PATH="$HOME/.local/bin:$PATH"
    - meson setup build-clang-tidy
    - meson compile -C build-clang-tidy
    - ninja -C build-clang-tidy clang-tidy
  needs: []
  when: manual
